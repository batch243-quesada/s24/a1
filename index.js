// console.log('Hello, World');

const getCube = (a, b = 3) => {
	console.log(`The cube of ${a} is ${Math.pow(a, b)}.`)
}

getCube(2);


const address = ['Eskinita Japan', 'Cebu City', 'Philippines'];
// Array Destructuring
const [street, city, country] = address;
console.log(`I live in ${street}, ${city}, ${country}.`);

const animal = {
	name: 'Lolong',
	species: 'saltwater crocodile',
	weight: 1075,
	measurement: '20 ft 3 in.'
}
// Object Destructuring
const {
	name,
	species,
	weight,
	measurement
} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} kgs with a measurement of ${measurement}`);

// forEach
let numArr = [1, 2, 3, 4, 5];
numArr.forEach(num => console.log(num));

// reduce
let reduceNumber = numArr.reduce((x, y) => x + y);
console.log(reduceNumber);

// Class-based Object
class Dog {
	constructor(name, age, breed) {
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

let dog = new Dog('Frankie', 5, 'Miniature Dachshund');
console.log(dog);


